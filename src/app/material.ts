import {MatButtonModule} from '@angular/material';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import { NgModule } from '@angular/core';
import {MatTreeModule} from '@angular/material/tree';
import {MatCardModule} from '@angular/material/card';

@NgModule ({
  imports: [MatButtonModule, MatSidenavModule, MatToolbarModule, MatIconModule, MatMenuModule, MatTreeModule, MatCardModule],
  exports: [MatButtonModule, MatSidenavModule, MatToolbarModule, MatIconModule, MatMenuModule, MatTreeModule, MatCardModule],
})

export class MaterialModule { }
