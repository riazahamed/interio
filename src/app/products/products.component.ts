import { Component, OnInit } from '@angular/core';
import {trigger,style,transition,animate,keyframes,query,stagger} from '@angular/animations';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
  animations: [
    trigger('listAnimations', [
      transition('* => *', [
        query(':enter', style({opacity: 0, }), {optional: true}),
        query(':enter', stagger('300ms', [
          animate('1s ease-in', keyframes([
            style({opacity: 0, transform: 'translateY(35px)', offset: 0}),
            style({opacity: 0.5, transform: 'translateY(15px)', offset: 0.3}),
            style({opacity: 1, transform: 'translateY(0)', offset: 1}),
          ]))
        ]), {optional: true})
      ])
    ])
  ]
})
export class ProductsComponent implements OnInit {

  products = [];
  
  constructor() { 
    this.products = [
      {
        src: '../../../images/1.png',
        name: 'Herman Miller',
        title: '"Contempo" Side Table',
        price: '1200.00 - £3000.00'
      },
      {
        src: '../../../images/2.png',
        name: 'Herman Miller',
        title: '"Contempo" Side Table',
        price: '1200.00 - £3000.00'
      },
      {
        src: '../../../images/3.png',
        name: 'Herman Miller',
        title: '"Contempo" Side Table',
        price: '1200.00 - £3000.00'
      },
      {
        src: '../../../images/1.png',
        name: 'Herman Miller',
        title: '"Contempo" Side Table',
        price: '1200.00 - £3000.00'
      },
      {
        src: '../../../images/2.png',
        name: 'Herman Miller',
        title: '"Contempo" Side Table',
        price: '1200.00 - £3000.00'
      },
      {
        src: '../../../images/3.png',
        name: 'Herman Miller',
        title: '"Contempo" Side Table',
        price: '1200.00 - £3000.00'
      },
      {
        src: '../../../images/1.png',
        name: 'Herman Miller',
        title: '"Contempo" Side Table',
        price: '1200.00 - £3000.00'
      },
      {
        src: '../../../images/2.png',
        name: 'Herman Miller',
        title: '"Contempo" Side Table',
        price: '1200.00 - £3000.00'
      }
    ]
  }

  ngOnInit() {
    
  }

}
